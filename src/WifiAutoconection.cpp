#include <ESP8266WiFi.h>          //https://github.com/esp8266/Arduino
#include <DNSServer.h>
#include <ESP8266WebServer.h>
#include <WiFiManager.h>         //https://github.com/tzapu/WiFiManager

class WifiAutoconection {
  private:
    WiFiManager wifiManager;
    
  public:
    boolean autoConnect() {
        Serial.println("Antes de llamar al wifiManager\n");

        return wifiManager.autoConnect("ESP8266");

    }

    void resetSettings(){
      wifiManager.resetSettings();
    }

    String getConfigPortalSSID(){
        return wifiManager.getConfigPortalSSID();
    }
};

