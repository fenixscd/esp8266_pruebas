#include <Arduino.h>
#include <LiquidCrystal_I2C.h>
#include <SimpleTimer.h>
#include "Reloj.cpp"
#include "TemperatureSensor.cpp"
// #include "WifiAutoconection.cpp"
#include "Pulsador.cpp"

SimpleTimer timer;
Reloj reloj;

#define DHTPIN D5
#define DHTTYPE DHT22

void readTemperature();
void initLCD();
void relojNoEncontrado();
void printLCDTime();

TemperatureSensor temperatureSensor(DHTPIN);
// LiquidCrystal_I2C lcd(0x27,20,4);  // set the LCD address to 0x27 for a 16 chars and 2 line display
// #define LCD_COLUMNS 16
// #define LCD_ROWS 2
#define LCD_ADDRESS 0x27
#define LCD_COLUMNS 20
#define LCD_ROWS 4

LiquidCrystal_I2C lcd(LCD_ADDRESS, LCD_COLUMNS, LCD_ROWS);

// WifiAutoconection wifiAutoconection;
Pulsador pulsadorOcho(D8);

// -------- Medidor de distancia -----------------------------------

#define PIN_ECHO D3
#define PIN_TRIG D4

float tiempo;
float distancia;

void initMedidor(){
  pinMode(PIN_TRIG, OUTPUT);
  pinMode(PIN_ECHO, INPUT);
}

float medir(){
  digitalWrite(PIN_TRIG, LOW);  //para generar un pulso limpio ponemos a LOW 4us
  delayMicroseconds(4);

  digitalWrite(PIN_TRIG, HIGH);  //generamos Trigger (disparo) de 10us
  delayMicroseconds(10);
  digitalWrite(PIN_TRIG, LOW);

  tiempo = pulseIn(PIN_ECHO, HIGH);
  distancia = tiempo/58.3;

  return distancia;
}

void printDistancia(float medicion){
  lcd.setCursor(0, 2);       // Colocar el cursor (fila(0|16), columna(0|1))
  lcd.print("Disp: " + String(medicion));      // Escribir el texto
}

// ------------------------------------

void setup()
{
  Serial.begin(115200);
  initLCD();
  initMedidor();

  timer.setInterval(1000, readTemperature);
  timer.setInterval(1000, printLCDTime);
}

void loop()
{
  timer.run();
  if (pulsadorOcho.read()){
    printDistancia(medir());

    lcd.setCursor(0, 0);       // Colocar el cursor (fila(0|16), columna(0|1))
    lcd.print(reloj.getStringTime());      // Escribir el texto
  }
  
}

String firstLine = "Temp: ";
float temperature = 0;
float newTemperature = 0;

void readTemperature(){

  temperature = temperatureSensor.readTemperature();
  if (temperature != newTemperature)
  {
    firstLine = "Temp: " + String(temperature);
        lcd.setCursor(0, 1);       // Colocar el cursor (fila(0|16), columna(0|1))
        lcd.print(firstLine);      // Escribir el texto
    Serial.println(firstLine);
    newTemperature = temperature;
  }
}

void initLCD()
{
  Serial.print("initLCD");
  lcd.init();      // Inicializar la pantalla
  lcd.backlight(); // Activar la luz de fondo de la pantalla
  lcd.clear();
  lcd.setCursor(0, 0); // Colocar el cursor en la primera columna de la primera fila
}

void printLCDTime(){
  lcd.setCursor(0, 0);       // Colocar el cursor (fila(0|16), columna(0|1))
  lcd.print(reloj.getStringTime());
}
