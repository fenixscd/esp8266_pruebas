#include <SPI.h>
#include <Wire.h>
#include <RTClib.h>

class Reloj {
private:
    RTC_DS3231 rtc;
    DateTime fechaPC = DateTime(__DATE__, __TIME__);
    boolean iniciarReloj(){
        if (!rtc.begin()) {
            Serial.println("Reloj no encontrado");
            while (1){
                delay(5000);
            }
        }
        return true;
    }


public:
    Reloj() {
        if (iniciarReloj()){
            if(fechaPC.year() != rtc.now().year()){
                rtc.adjust(DateTime(__DATE__, __TIME__));
            }
        }
    }

    void setDateTime(int year, int month, int day, int hour, int minute, int second) {
        DateTime dateTime = DateTime(year, month, day, hour, minute, second);
        rtc.adjust(dateTime);
    }

    void printDateTime() {
        Serial.println(getStringDataTime());
    }

    String getStringDataTime(){
        String StringDataTime = 
            getStringData() + " " + getStringTime();
        return StringDataTime;
    }

    String getStringData(){
        int year= rtc.now().hour();
        int month = rtc.now().minute();
        int day = rtc.now().second();
        // Crear un búfer para el formato
        char buf[14];

        // Formatear de la fecha en el búfer
        sprintf(buf, "%04d/%02d/%02d", year, month, day);

        // Crear un objeto String a partir del búfer formateado
        String StringData(buf);
        return StringData;
       
    }

    String getStringTime(){
        int horas = rtc.now().hour();
        int minutos = rtc.now().minute();
        int segundos = rtc.now().second();
        // Crear un búfer para el formato
        char buf[12];

        // Formatear la hora en el búfer
        sprintf(buf, "%02d:%02d:%02d", horas, minutos, segundos);

        // Crear un objeto String a partir del búfer formateado
        String tiempo_actual(buf);
        return tiempo_actual;
    }
};
