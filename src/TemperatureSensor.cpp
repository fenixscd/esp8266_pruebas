#include <DHT.h>
#include <DHT_U.h>

class TemperatureSensor {
  private:
    int pin; // el pin al que está conectado el sensor DHT22
    DHT dht; // instancia del sensor DHT22
  public:
    TemperatureSensor(int pin) : pin(pin), dht(pin, DHT22) {
      // configurar el sensor DHT22
      dht.begin();
    }
    float readTemperature() {
      // leer la temperatura del sensor DHT22
      float temperature = dht.readTemperature();
      if (isnan(temperature)) {
        // si no se pudo leer la temperatura, devolver un valor negativo
        return -1.0f;
      }
      return temperature;
    }
};
