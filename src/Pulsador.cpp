#include <Arduino.h>

class Pulsador {
  private:
    int pin;
    unsigned long tiempoActual = 0;
    unsigned long tiempoAnterior = 0;
    unsigned long tiempoRebote = 400;
    int pulsado = 1;
  public:
    Pulsador(int p) {
        pin = p;
        pinMode(p, INPUT);
    }

    boolean read(){
        int actualState = digitalRead(pin);
        
        if (actualState == pulsado){
          if(isNotRebot()){
            Serial.println("Presionado");
            return true;
          }
        }
      return false;
    }

    boolean isNotRebot(){
      tiempoActual = millis();
      if((tiempoActual - tiempoAnterior) > tiempoRebote){
        tiempoAnterior = tiempoActual;
        return true;
      }
      return false;
    }
};
